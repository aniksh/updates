\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}

\title{Summaries of Publications on Word and Sentence Embedding}
\author{Anik Saha}
%\date{October 09, 2018}
\begin{document}
\maketitle

\section{Word Representations via Gaussian Embedding (ICML 2015) -  
    [Luke Vilnis, Andrew McCallum]}

\subsection{Overview}
This paper advocates for density-based distributed embeddings and presents 
a method for learning representations in the space of Gaussian distributions.
Mapping a word to density provides many interesting advantages, including 
better uncertainty information about a representation and its relationships, 
expressing asymmetries more naturally than dot product or cosine similarity.
They explore Gaussian function embeddings (with diagonal covariance), in which 
both means and variances are learned from data. 

\subsection{Model}
The model is trained with a ranking-based loss (Expected likelihood kernel)
\[
L_m(w, c_p, c_n) = max(0, m - E(w, c_p) + E(w, c_n))
\]
where $c_p$ and $c_n$ are negative context words for the word $w$. \\

The energy function is
\[
E (f,g) = \int_{x \in \mathbb{R}^n} f(x) g(x) dx
\]

For Gaussians, the inner product is
\[
E(P_i, P_j) = \int_{x \in \mathbb{R}^n} \mathcal{N}(x; \mu_i, \Sigma_i) 
\mathcal{N}(x; \mu_j, \Sigma_j) dx = \mathcal{N}(0; \mu_i -\mu_j, \Sigma_i - \Sigma_j)
\]
So, there is a closed form for the logarithm of the energy function. \\

KL-divergence is another choice for the energy function
\begin{align*}
- E(P_i, P_j) &= D_{KL} (\mathcal{N}_j || \mathcal{N}_j) = 
\int_{x \in \mathbb{R}^n} \mathcal{N}(x; \mu_i, \Sigma_i) 
\log \frac{\mathcal{N}(x; \mu_j, \Sigma_j)}{\mathcal{N}(x; \mu_i, \Sigma_i)}dx \\
&= \frac{1}{2} \left(tr(\Sigma_i^{-1} \Sigma_j) + (\mu_i - \mu_j)^T \Sigma_i^{-1} (\mu_i - \mu_j)
 - d - \log \frac{det(\Sigma_j)}{det(\Sigma_i)} \right)
\end{align*}

For regularization, they use two hard constraints
\[
||\mu_i||_2 \le C, \forall i \qquad and \qquad mI < \Sigma_i < MI, \forall i
\]

\subsection{Experiments}
They train two models with two loss functions using both spherical and diagonal 
covariance matrices. For evaluating word similarity and entailment, they learn 
the embeddings using the ranking-based loss. In both tasks, the spherical 
variance achieves better performance than the diagonal variance specially in the
similarity measure. Similarity is measured using the cosine distance between 
the means and the cosine distance between distributions (Expected likelihood 
inner product). \\

They also embed a tree hierarchy using the KL-divergence between parents and 
children. Negative contexts come from randomly sampled nodes that are neither 
ancestors nor descendents, while positive contexts come from ancestors or 
descendents using the appropriate directional KL divergence.
\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{figs/gaussian-tree}
\end{figure}

\subsection{Future Work}
Using multimodal distribution is a clear area to work for probabilistic 
embeddings. We can also try to use other distributions to represent word 
embeddings.


\section{Multimodal Word Distributions (ACL 2017) - [Ben Athiwaratkun, Andrew 
    Gordon Wilson]}
 
\subsection{Overview}
Word embeddings in a vector space are point representations containing semantic 
information. This paper introduces multimodal word distributions formed from 
Gaussian mixtures, useful for multiple word meanings, entailment and rich 
uncertainty information. They propose an energy-based max-margin objective to 
learn this distribution.

\begin{figure}[h]
    \centering
    \includegraphics[width=.5\linewidth]{figs/gmm-embed}
    \caption{Top: Gaussian mixture embedding that has different Gaussian 
    component for different meanings of the word 'rock'. Bottom: Gaussian 
    embedding where words with multiple meaning have a high variance to cover  
    the different meanings}
\end{figure}

\subsection{Method}
The max-margin ranking objective
\[
L_\theta (w,c,c') = \max(0, m - \log E_\theta (w,c) + \log E_\theta (w,c'))
\]
where $c$ is a context word and $c'$ is a negative sample (out of context) word 
for $w$.  The similarity of a word and its positive context are pushed higher 
than that of its negative context by the margin $m$. They use subsampling and 
negative sampling from $word2vec$.

The energy function is 
\[
E (f,g) = \int f(x) g(x) dx = \left< f,g \right >_{L_2}
\]
where $\left< \right >_{L_2}$ is the inner product in the Hilbert space $L_2$. 
This energy function has a closed form for Gaussian mixtures. So it is a 
better choice than KL divergence.

\subsection{Experiment}
They train the model on a concatenation of two datasets: UKWAC (2.5 billion 
tokens) and Wackypedia (1 billion tokens). The experiments are done with 2 
Gaussian components and $m=1$. \\

For word similarity, they use maximum cosine 
similarity 
and minimum Euclidean distance between the means of the Gaussian components of 
different words. They used Spearman correlation between predicted scores and 
true labels to evaluate word similarity. Compared to skip-gram from word2vec 
and Gaussian embedding from (Word Representations via Gaussian Embedding
- Luke Vilnis, Andrew McCallum) their model got better similarity scores for 
9 out of 11 datsets they tested.

\subsection{Future Work}
This multimodal word distribution can be used in a new type of language model 
that can leverage the information in the distribution. We can combine 
the word distributions in a sentence to generate sentence embeddings as 
distributions.

\section{Probabilistic FastText for Multi-Sense Word Embeddings (ACL 2018) - 
    [Ben Athiwaratkun] et el.}

\subsection{Overview}
This work extends the Gaussian mixture model of embeddings. They represent each 
word with a Gaussian mixture density, where the mean of a mixture component is 
given by the sum of character n-grams. The concept of summing character n-grams 
to get word representation is based on \textsc{FastText}. This allows the model 
to share statistical strength across sub-word structures, producing accurate 
representations of rare, misspelt, or unseen words.

\subsection{Method}
A word $w$ is associated with a density function
\[
f(x) = \sum_{i=1}^K p_{w,i}\mathcal{N}(x;\mu_{w,i}, \Sigma_{w,i})
\]
where $\mu_{w,i}$ are the mean vectors and $\Sigma_{w,i}$ are the covariance 
matrices and $\sum_{i=1}^K p_{w,i} = 1$. \\

For word $w$, the mean vector $\mu_w$ is estimated with the average over n-gram 
vectors and its dictionary-level vector. 
\[
\mu_w = \frac{1}{|NG_w| + 1} \left( v_w + \sum_{g\in NG_w} z_g \right)
\]
where $z_g$ is the vector character n-gram $g$, $v_w$ is the word vector and 
$NG_w$ is the set of character n-grams for the word $w$. \\

Examples of 3,4-grams for a word "beautiful", including the beginning-of-word 
character '$<$' and end-of-word character '$>$', are:
\begin{itemize}
    \item 3-grams: $<$be, bea, eau, aut, uti, tif, ful, ul$>$
    \item  4-grams: $<$bea, beau .., iful ,ful$>$
\end{itemize}

They use the same loss function as the Gaussian mixture paper. 
\[
L(f, g) = max [0, m - E(f, g) + E(f, n)]
\]

They have a simplified energy function where they assume spherical covariance 
instead of a diagonal covariance and use the scale of covariance matrix as a 
hyperparameter.

\subsection{Experiments}
To evaluate the similarity between two words they compute the maximum cosine 
similarity among the mean vectors of the words. For 300 dimensional embedding,
the previous Gaussian mixture model (W2GM) was better than this model but 
this model was better for 50 dimensions. On the SCWS dataset (contains words 
with multiple meanings) this model is better than W2GM.

\subsection{Future Work}
This idea of computing word embedding from character n-grams can be extended 
to generating sentence embeddings from word or n-gram embeddings. The 
multimodal probability distribution may distinguish sentences with 
different word orders.

\section{Embedding Words as Distributions with a Bayesian Skip-gram Model 
    (Computational Linguistics 2018)- [Arthur Brazinskas, Serhii Havrylov, Ivan Titov]}

\subsection{Overview}
In this Bayesian model the authors generate probability density for a word 
from a word-specific prior density. The prior density encodes the distribution 
of a word's potential 'meanings'. They obtain context-specific densities which 
encode uncertainty about the sense of a word given its context and correspond 
to the approximate posterior distributions within their model. The  estimation
method is based on the variational autoencoding framework.

\section{A Structured Self-Attentive Sentence Embedding (ICLR 2017) - 
    [Zhouhan Lin, Yoshua Bengio et el.]}

\subsection{Overview}
This paper uses a 2-D matrix to represent sentence embedding instead of a vector. 
They propose a self-attention mechanism and a regularization term for the model.
Sentence embedding is learned during a supervised learning task.

\subsection{Model}
The proposed sentence embedding model consists a bidirectional LSTM, and a 
self-attention mechanism, which provides a set of summation weight vectors 
for the LSTM hidden states.  The weighted LSTM hidden states are considered 
as an embedding for the sentence. Concatenating the forward and backward 
hidden states of the LSTM, they obtain $\mathbf{h}_t$

They get a vector of weights $\mathbf{a}$ of size $n$ using 
\[
\mathbf{a} = softmax(\mathbf{w_{s2}} \tanh(\mathrm{W_{s1}} \mathrm{H}^T)
\]
where $\mathrm{H} = \begin{bmatrix}
\mathbf{h}_1 & \mathbf{h}_2 & \dots & \mathbf{h}_n
\end{bmatrix}^T$

They use the weight vector $a$ to combine the hidden vectors in $\mathrm{H}$
and generate a vector representation $\mathbf{m}$ of the sentence. To 
get a matrix representation of the sentence, they use a matrix of weights
\[
\mathrm{A} = softmax(\mathrm{W_{s2}} \tanh(\mathrm{W_{s1}} \mathrm{H}^T)
\]
where $\mathrm{A}$ is of size $r\times n$ and it is multiplied by 
$\mathrm{H}$ to get the sentence embedding
\[
A = MH
\]

To encourage the diversity of summation weight vectors across different 
hops of attention, they introduce a penalty term
\[
P = || \mathrm{AA^T} - \mathrm{I}||_F^2
\]

So, they are encouraging the different weight vectors $\mathbf{a}_i$, 
$\mathbf{a}_j$ to be orthogonal to each other.

\subsection{Experiments}
For author profiling of tweets and sentiment analysis on Yelp, they 
reported better results than two baselines with BiLSTM and CNN with max 
pooling in both models. \\

For textual entailment on SNLI dataset, their accuracy was 0.2\% lower 
than the state-of-the-art in 2016. For the pair of sentences in this 
task, they shared the weights in the BiLSTM and attention MLP. \\

The number of rows in the matrix embedding has a positive effect on 
the performance in these tasks as shown below. Accuracy on Age and 
Yelp dataset increases with the number of rows.
\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{figs/matrix-embed}    
\end{figure}
Different rows of the matrix embedding focuses on different aspects of 
semantics of a sentence.

\subsection{Future Work}
This concept can be applied to longer content like paragraph, document 
using sentence embeddings.

\section{Unsupervised Learning of Sentence Embeddings using Compositional 
    n-Gram Features (NAACL-HLT 2018) - [Matteo Pagliardini, Prakhar Gupta, 
    Martin Jaggi]}

\subsection{Overview}
A simple unsupervised model allowing to compose sentence embeddings using 
word vectors along with n-gram embeddings, simultaneously training 
composition and the embedding vectors themselves. The computational
complexity of this embedding is O(1) vector operations per word processed, 
both during training and inference of the sentence embeddings.

\subsection{Model}
The model can be interpreted as a natural extension of the word-contexts 
from C-BOW. They learn a source (or context) embedding $\mathbf{v}_w$ and target 
embedding $\mathbf{u}_w$ for each word $w$ in the vocabulary, with embedding 
dimension $h$. The sentence embedding $\mathbf{v}_S$ for $S$ is modeled as
\[
\mathbf{v}_S := \frac{1}{|R(S)|} \sum_{w \in R(S)} \mathbf{v}_w
\]
where $R(S)$ is the list of n-grams present in the sentence $S$. They use 
subsampling for the target words and negative sampling for the loss function 
\[
\min_{U,V} \sum_{S \in \mathcal{C}} \sum_{w_t \in S} (l(\mathbf{u}_{w_t}^T 
\mathbf{v}_{S\setminus w_t}) + \sum_{w' \in N{w_t}} l(-\mathbf{u}_{w'}^T 
\mathbf{v}_{S\setminus w_t}))
\]
where $l(x)=\log(1+e^{-x})$ is the logistic loss function. For higher order 
n-grams, hashing trick is used similar to \textsc{FastText}.

\subsection{Experiments}
Three different datasets have been used to train the models: the Toronto book 
corpus (0.9B words), Wikipedia sentences (1.7B words) and tweets (19.7B words). \\
They get state-of-the-art result in 2 out of 6 supervised classification 
tasks and comparable results in other tasks. In the unsupervised similarity 
evaluation task, they get the best correlation measure in 4 out of 7 tasks.
Addition of bigrams to the models doesn’t help much when it comes to 
unsupervised evaluations but gives a significant boost-up in accuracy on
supervised tasks.

\subsection{Future Work}
We can augment the model with trigrams for capturing more compositional 
and order information. We can use a weighted sum of the n-grams instead 
of averaging.

\section{Embedding Syntax and Semantics of Prepositions via Tensor
    Decomposition (NAACL-HLT 2018) - [Hongyu Gong, Suma Bhat, Pramod Viswanath]}

\subsection{Overview}
Prepositions are some of the most frequent words in English and play important 
roles in the syntax and semantics of sentences. This paper derives preposition
embeddings via tensor decomposition on a large unlabeled corpus. They reveal 
a new geometry involving Hadamard products and empirically demonstrate its 
utility in paraphrasing phrasal verbs.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\linewidth]{figs/prepos}
\end{figure}

\subsection{Method}
They generate a third order tensor $X_{N×N×(K+1)}$ from the WikiCorpus, where 
$N$ is the vocabulary size without the prepositions and $K$ is the number of 
prepositions for the specific task. Two words co-occur if they appear within a
distance $t$ of each other in a sentence. $X$ is the co-occurance tensor. The 
window size is $t=3$. They add an extra slice $X[:, :, K + 1]$, where the entry 
$X_{ij(K+1)}$ is the number of occurrences where $w_i$ co-occurs with $w_j$ 
(within distance $2t = 6$) but at least one of them is not within a distance 
of $t$ of any preposition. \\

Two methods are used for low rank approximation of $\log(1+X)$.

\begin{enumerate}
\item \textit{ALS:}
The tensor $\log(1 + X)$ is decomposed into three modes: $U_{d\times N}$ , $W_{d\times N}$ 
and $Q_{d\times(K+1)}$, based on the solutions to the optimization problem 
\[
L = \min_{U,W,Q} \sum_{i=1}^N \sum_{j=1}^N \sum_{k=1}^{K+1} (\left< \mathbf{u}_i,
\mathbf{w}_j \mathbf{q}_k \right> - \log(1+X_{ijk}))^2
\]
They use a variant of ALS, Orth-ALS which periodically orthogonalizes the 
decomposed components while fixing two modes and updating the remaining one.

\item \textit{Weighted Decomposition:}
They employ the GloVe objective function to reduce the dynamic range of the tensor and 
minimize the objective function
\[
L_{weighted} = \min_{U,W,Q} \sum_{i=1}^N \sum_{j=1}^N \sum_{k=1}^{K+1} w_{ijk} 
(\left< \mathbf{u}_i, \mathbf{w}_j \mathbf{q}_k \right> + b_{U_i} + b_{W_j} + b_{Q_k} 
- \log(1+X_{ijk}))^2
\]
where weight $w_{ijk} = \min \left( \left( \frac{X_{ijk}}{x_{max}}\right)^\alpha, 1 \right)$ 
with $x_{max} = 10$ and $\alpha=0.75$.
\end{enumerate}

\subsection{Experiments}
They achieve state-of-the-art results on preposition selection and comparable result 
on preposition attachment with their embeddings.

\subsection{Conclusion}
Since the number of prepositions is low and they are very frequent, the sparsity of 
the tensor is similar to the word co-occurance matrix. So the tensor decomposition method 
was effective for obtaining embeddings of prepositions.



\end{document}



















